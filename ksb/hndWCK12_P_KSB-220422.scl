﻿FUNCTION_BLOCK "hndWCK12_P_KSB"
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT 
      hwKSBRotoDynamicRD : HW_SUBMODULE;   //  KSB RotoDynamic hardware module
      hwKSBRotoDynamicWR : HW_SUBMODULE;   //  KSB RotoDynamic hardware module
      hwKSBDriveMotor : HW_SUBMODULE;   //  KSB Drive + Motor module
      HWI_MotorDisconnectSwitch : Bool;   //  Motor local disconnect switch in the field
      HWI_DriveContactorSwitch : Bool;   //  Auxiliary switch on the drive contactor
      HWI_DriveFault : Bool;   //  Fault signal from drive
      Auto_RampUPRange { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;   //  Rampup Range if CustomRampON
      Auto_RampUPTime { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Int;   //  Rampup Time if CustomRampON
      Auto_RampDOWNRange { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;   //  Rampdown Range if CustomRampON
      Auto_RampDOWNTime { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Int;   //  Rampdown Time if CustomRampON
      HWI_Speed { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Int;   //  Field switch
      DRIVE_HW_Diag { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Siemens input  module diagnostics
      GlobalAlarmAck { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Global Acknowledge
      EnableBlkIconControl { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Enable Block Icon Control
   END_VAR
   VAR_INPUT DB_SPECIFIC
      SS_Value { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;   //  Standby Synchronization Value
   END_VAR
   VAR_INPUT 
      InterlockMsg { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : String;   //  Interlock Message
      FeedbackDirection { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  FALSE:4-20ma=0-100% TRUE 4-20mA=100-0%
   END_VAR

   VAR_OUTPUT 
      HWO_PumpMV { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Int;   //  Pump drive setpoint
      PumpRunning { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Pump speed is over the PumpRunningPercentage
      PumpStopped { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Pump speed is below the PumpStoppedPercentage
      PumpActSpd : Real;
      PumpSetpoint : Real;
   END_VAR

   VAR_IN_OUT 
      KSB : "udtKSB_Pump";
      EPICS : "DEVTYPE_ICS_WCK12_P_KSB";   //    EPICS interface for KSB Pump
      STAT : "hndWCK12_P_KSB_STAT";
      ForcedCounter : "udtForcedCounter";
      AutoStart { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Start the pump in automatic mode
      AutoStop { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Stop the pump in automatic mode
      Auto_SetPoint { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;   //  Pump Setpoint in Automatic Mode
      ForceAutoMode { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Force to Auto Mode
      ForceManualMode { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Force to Manual Mode
      StartInterlock { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Inhibit pump moving in auto/manual mode
      Auto_CustomRampON_OFF { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Enable external Ramp parameters
   END_VAR
   VAR_IN_OUT DB_SPECIFIC
      SS_ON { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;   //  Standby Synchronization ON/OFF
   END_VAR

   VAR 
      Risingedge_ForceManual {InstructionName := 'R_TRIG'; LibVersion := '1.0'; ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : R_TRIG;   //  Edge detection for Manual Mode
      Risingedge_ForceAuto {InstructionName := 'R_TRIG'; LibVersion := '1.0'; ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : R_TRIG;   //  Edge detection for Auto Mode
      FallingEdge_DriveAuto {InstructionName := 'F_TRIG'; LibVersion := '1.0'} : F_TRIG;
      Drive_AutoMode { S7_SetPoint := 'True'} : Bool;
   END_VAR

   VAR_TEMP 
      UnScaleRetVal : Word;
      ScaleRetVal : Word;
      RampSpeed : Real;
      IsDiscrepancy : Bool;
      iStKSB_RDPWRRetVal : Int;
      iTmpSetpoint : UInt;
      rMemSetPoint : Real;
      iTmpFeedback : UInt;
      iStKSB_RDPRDRetVal : Int;
      iStKSB_DMRetVal : Int;
   END_VAR


BEGIN
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se
	//
	//Functionality: Remote controlled analog pump handler function block
	// 
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//04.00.00 2021-03-10   ESS/ICS               Outsourced all Static variable to the Handle_Pump_P_STAT, , so the Pump remains status during EPICS update
	//03.00.00 2020-04-28   ESS/ICS               Added SS_ON/SS_Value and Output limits 
	//02.00.00 2018-10-17   ESS/ICS               Updated ramping 
	//01.00.00 2018-02-05   ESS/ICS               First version 
	//=============================================================================
	//
	//
	//
	
	// Installation note for Samson speeder
	// The physical switch on the speeder changes the local feedback display direction
	// Parameter 7 schanges the SP direction
	// Paramter 29 changes the PLC feedback direction
	
	
	//Freeze parameters during PLC download of the PLCFactory .scl files.
	IF (NOT "Utilities".ParameterFreeze) THEN
	    #STAT.P_ForceSpeed := #EPICS.P_ForceSpeed;
	    #STAT.P_Setpoint := #EPICS.P_Setpoint;
	    #STAT.P_Manipulated := #EPICS.P_Manipulated;
	    #STAT.P_Step := #EPICS.P_Step;
	    #STAT.P_RampUPTIME := #EPICS.P_RampUPTIME;
	    #STAT.P_RampUPRANGE := #EPICS.P_RampUPRANGE;
	    #STAT.P_RampDNTIME := #EPICS.P_RampDNTIME;
	    #STAT.P_RampDNRANGE := #EPICS.P_RampDNRANGE;
	END_IF;
	
	
	//Force Mode Edge Detection
	#Risingedge_ForceAuto(CLK := #ForceAutoMode);
	#Risingedge_ForceManual(CLK := #ForceManualMode);
	
	
	//Limit the Setpoint both Manual and Auto mode
	#STAT.Auto_SetPoint_Limit := #Auto_SetPoint;
	#EPICS.SPLimitActive := FALSE;
	IF (#Auto_SetPoint < #STAT.P_SPD_MIN) THEN
	    #STAT.Auto_SetPoint_Limit := #STAT.P_SPD_MIN;
	    IF (#STAT.OpMode_Auto) THEN
	        #EPICS.SPLimitActive := TRUE;
	    END_IF;
	END_IF;
	IF (#Auto_SetPoint > #STAT.P_SPD_MAX) THEN
	    #STAT.Auto_SetPoint_Limit := #STAT.P_SPD_MAX;
	    IF (#STAT.OpMode_Auto) THEN
	        #EPICS.SPLimitActive := TRUE;
	    END_IF;
	END_IF;
	
	IF (#STAT.P_Setpoint < #STAT.P_SPD_MIN) THEN
	    IF (#STAT.OpMode_Manual) THEN
	        #EPICS.SPLimitActive := TRUE;
	    END_IF;
	    #STAT.P_Setpoint := #STAT.P_SPD_MIN;
	END_IF;
	IF (#STAT.P_Setpoint > #STAT.P_SPD_MAX) THEN
	    IF (#STAT.OpMode_Manual) THEN
	        #EPICS.SPLimitActive := TRUE;
	    END_IF;
	    #STAT.P_Setpoint := #STAT.P_SPD_MAX;
	END_IF;
	
	//HMI Faceplate - OP MODES - AUTO
	IF ((#EPICS.Cmd_Auto) OR #Risingedge_ForceAuto.Q) AND NOT #SS_ON THEN
	    #STAT.OpMode_Auto := TRUE;
	    #STAT.OpMode_Manual := FALSE;
	    #STAT.OpMode_Forced := FALSE;
	END_IF;
	
	//HMI Faceplate - OP MODES - MANUAL
	IF ((#EPICS.Cmd_Manual OR #Risingedge_ForceManual.Q OR #SS_ON) AND (#EPICS.Inhibit_Manual = FALSE)) THEN
	    #STAT.OpMode_Auto := FALSE;
	    #STAT.OpMode_Manual := TRUE;
	    #STAT.OpMode_Forced := FALSE;
	    #STAT.SP_BeforeSwithover := #STAT.P_Setpoint;
	END_IF;
	
	////HMI Faceplate - OP MODES - FORCED
	IF (#EPICS.Cmd_Force AND #EPICS.Inhibit_Force = FALSE) THEN
	    #STAT.OpMode_Auto := FALSE;
	    #STAT.OpMode_Manual := FALSE;
	    #STAT.OpMode_Forced := TRUE;
	    #STAT.FB_ForceSpeed := #STAT.PumpSpeed;
	    #STAT.FB_Manipulated := #STAT.PumpMV;
	END_IF;
	
	//If there is no Operation Mode, set the mode to Manual
	IF (#STAT.OpMode_Auto = FALSE) AND (#STAT.OpMode_Manual = FALSE) AND (#STAT.OpMode_Forced = FALSE) THEN
	    #STAT.OpMode_Auto := FALSE;
	    #STAT.OpMode_Manual := TRUE;
	    #STAT.OpMode_Forced := FALSE;
	END_IF;
	
	
	
	//=============================================================================
	// Check Ramping condition
	IF (#STAT.P_RampUPTIME > 0) AND (#STAT.P_RampUPRANGE > 0.0) AND (#STAT.P_RampDNTIME > 0) AND (#STAT.P_RampDNRANGE > 0.0) THEN
	    #STAT.RampSettingOK := TRUE;
	ELSE
	    #STAT.RampSettingOK := FALSE;
	    #STAT.Ramping := FALSE;
	    #STAT.ManualRamping := FALSE;
	END_IF;
	
	
	//Turn ON Ramping
	IF (#STAT.OpMode_Auto) THEN
	    IF (#Auto_CustomRampON_OFF) THEN
	        IF (#Auto_RampUPTime = 0) AND (#Auto_RampUPRange = 0.0) AND (#Auto_RampDOWNTime = 0) AND (#Auto_RampDOWNRange = 0.0) THEN
	            //Feedback
	            #STAT.FB_RampUPRANGE := #STAT.Default_RampUpRange;
	            #STAT.FB_RampUPTIME := #STAT.Default_RampUpTime;
	            #STAT.FB_RampDNRANGE := #STAT.Default_RampDownRange;
	            #STAT.FB_RampDNTIME := #STAT.Default_RampDownTime;
	        ELSE
	            //Feedback
	            #STAT.FB_RampUPRANGE := #Auto_RampUPRange;
	            #STAT.FB_RampUPTIME := #Auto_RampUPTime;
	            #STAT.FB_RampDNRANGE := #Auto_RampDOWNRange;
	            #STAT.FB_RampDNTIME := #Auto_RampDOWNTime;
	        END_IF;
	    ELSE
	        IF (NOT #STAT.RampSettingOK) THEN
	            //Feedback
	            #STAT.FB_RampUPRANGE := #STAT.Default_RampUpRange;
	            #STAT.FB_RampUPTIME := #STAT.Default_RampUpTime;
	            #STAT.FB_RampDNRANGE := #STAT.Default_RampDownRange;
	            #STAT.FB_RampDNTIME := #STAT.Default_RampDownTime;
	            
	        ELSE
	            //Feedback
	            #STAT.FB_RampUPRANGE := #STAT.P_RampUPRANGE;
	            #STAT.FB_RampUPTIME := #STAT.P_RampUPTIME;
	            #STAT.FB_RampDNRANGE := #STAT.P_RampDNRANGE;
	            #STAT.FB_RampDNTIME := #STAT.P_RampDNTIME;
	            
	        END_IF;
	    END_IF;
	    #STAT.Ramping := #Auto_CustomRampON_OFF;
	ELSE
	    //Feedback
	    #STAT.FB_RampUPRANGE := #STAT.P_RampUPRANGE;
	    #STAT.FB_RampUPTIME := #STAT.P_RampUPTIME;
	    #STAT.FB_RampDNRANGE := #STAT.P_RampDNRANGE;
	    #STAT.FB_RampDNTIME := #STAT.P_RampDNTIME;
	    
	    IF (#EPICS.Cmd_RampON) AND (#STAT.RampSettingOK) THEN
	        #STAT.ManualRamping := TRUE;
	    END_IF;
	    
	    //Turn OFF Ramping
	    IF (#EPICS.Cmd_RampOFF) THEN
	        #STAT.ManualRamping := FALSE;
	    END_IF;
	    #STAT.Ramping := #STAT.ManualRamping;
	END_IF;
	
	//=============================================================================
	//  READ Drive Parameters
	#iStKSB_RDPRDRetVal := DPRD_DAT(LADDR := #hwKSBRotoDynamicRD, RECORD => #KSB.udtKSB_RDP_RD);
	#iStKSB_DMRetVal := DPRD_DAT(LADDR := #hwKSBDriveMotor, RECORD => #KSB.udtKSB_DM);
	
	//  Detect change of Drive Auto mode
	IF (#KSB.udtKSB_RDP_RD.byInControl_Mode = 130) THEN
	    #Drive_AutoMode := TRUE;
	ELSE
	    #Drive_AutoMode := FALSE;
	END_IF;
	#FallingEdge_DriveAuto(CLK := #Drive_AutoMode);
	
	//=============================================================================
	//  Map Drive EPICS Parameters
	IF (#iStKSB_RDPRDRetVal = 0) THEN
	    
	    #EPICS.Running := #KSB.udtKSB_RDP_RD.bInRunning;
	    #EPICS.Ready := #KSB.udtKSB_RDP_RD.bInReady;
	    #EPICS.AutoMode := #KSB.udtKSB_RDP_RD.bInAuto_Mode;
	    #EPICS.Fault := #KSB.udtKSB_RDP_RD.bInFault;
	    #EPICS.BusControlMode := #KSB.udtKSB_RDP_RD.bInBus_Control_Mode;
	    #EPICS.PumpSpeedMax := #KSB.udtKSB_RDP_RD.bInPumpSpeedMax;
	    #EPICS.PumpSpeedMin := #KSB.udtKSB_RDP_RD.bInPumpSpeedMin;
	    
	    IF (#STAT.OpMode_Forced = FALSE) THEN   //  Only in PLC Auto or Manual Mode
	        (* #iTmpFeedback.%X8 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X0;
	        #iTmpFeedback.%X9 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X1;
	        #iTmpFeedback.%X10 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X2;
	        #iTmpFeedback.%X11 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X3;
	        #iTmpFeedback.%X12 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X4;
	        #iTmpFeedback.%X13 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X5;
	        #iTmpFeedback.%X14 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X6;
	        #iTmpFeedback.%X15 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement.%X7;
	        
	        #iTmpFeedback.%X0 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X0;
	        #iTmpFeedback.%X1 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X1;
	        #iTmpFeedback.%X2 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X2;
	        #iTmpFeedback.%X3 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X3;
	        #iTmpFeedback.%X4 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X4;
	        #iTmpFeedback.%X5 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X5;
	        #iTmpFeedback.%X6 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X6;
	        #iTmpFeedback.%X7 := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement_2.X7;
	        
	        #STAT.PumpSpd := #iTmpFeedback / 100; *)
	        #STAT.PumpSpd := #KSB.udtKSB_RDP_RD.byInFeedback_Measurement / 100;
	    END_IF;
	END_IF;
	
	#EPICS.PumpActSpeed := #KSB.udtKSB_DM.rInSpeed;
	#EPICS.PumpOUTFreq := #KSB.udtKSB_DM.rInOutputFrequency;
	#EPICS.PumpOUTVoltage := #KSB.udtKSB_DM.rInMotorVoltage;
	#EPICS.PumpOUTCurrent := #KSB.udtKSB_DM.rInMotorCurrent;
	#EPICS.PumpOUTPower := #KSB.udtKSB_DM.rInMotorInputPower;
	#EPICS.PumpPUTemp := #KSB.udtKSB_DM.rInHeatSinkTemperature;
	
	//AUTO MODE
	IF (#STAT.OpMode_Auto = TRUE) THEN
	    #EPICS.EnableAutoBtn := FALSE;
	    #EPICS.EnableManualBtn := TRUE;
	    #EPICS.EnableForcedBtn := TRUE;
	    
	    //SET the setpoint and start command
	    IF (#STAT.StartInterlock = FALSE) THEN
	        IF (#STAT.Ramping) THEN
	            #STAT.RampSetPoint := #STAT.Auto_SetPoint_Limit;
	        ELSE
	            #STAT.PumpSP := #STAT.Auto_SetPoint_Limit;
	        END_IF;
	        
	        #STAT.FB_Setpoint := #STAT.PumpSP;
	        
	        IF (#AutoStart AND NOT #STAT.StartInterlock) THEN
	            #STAT.Cmd_Run := TRUE;
	        END_IF;
	        IF (#AutoStop) THEN
	            #STAT.Cmd_Run := FALSE;
	        END_IF;
	        
	    END_IF;
	END_IF;
	
	//MANUAL MODE
	IF (#STAT.OpMode_Manual = TRUE) THEN
	    #EPICS.EnableAutoBtn := TRUE;
	    #EPICS.EnableManualBtn := FALSE;
	    #EPICS.EnableForcedBtn := TRUE;
	    
	    #EPICS.FB_Step := #STAT.P_Step;
	    IF (#EPICS.FB_Step < 0.1) THEN
	        #EPICS.FB_Step := 0.1;
	    END_IF;
	    
	    IF (#STAT.StartInterlock = FALSE) THEN
	        //SET the setpoint and start command
	        IF (#STAT.SP_BeforeSwithover > -1.0) THEN
	            IF (#STAT.SP_BeforeSwithover <> #STAT.P_Setpoint) THEN
	                #STAT.SP_BeforeSwithover := -1.0;
	            END_IF;
	        ELSE
	            #STAT.FB_Setpoint := #STAT.P_Setpoint;
	        END_IF;
	        IF (#STAT.Ramping) THEN
	            #STAT.RampSetPoint := #STAT.FB_Setpoint;
	        ELSE
	            #STAT.PumpSP := #STAT.FB_Setpoint;
	        END_IF;
	        
	        IF (#EPICS.Cmd_ManuStart AND NOT #STAT.StartInterlock) THEN
	            #STAT.Cmd_Run := TRUE;
	        END_IF;
	        IF (#EPICS.Cmd_ManuStop) THEN
	            #STAT.Cmd_Run := FALSE;
	            #STAT.PumpSP := 0;
	        END_IF;
	    ELSE
	        #STAT.SP_BeforeSwithover := #STAT.P_Setpoint;
	        #STAT.FB_Setpoint := #STAT.PumpSP;
	    END_IF;
	END_IF;
	
	#ForcedCounter.ForcedMax := #ForcedCounter.ForcedMax + 1;
	
	
	//FORCE MODE
	IF (#STAT.OpMode_Forced = TRUE) THEN
	    #EPICS.EnableAutoBtn := TRUE;
	    #EPICS.EnableManualBtn := TRUE;
	    #EPICS.EnableForcedBtn := FALSE;
	    
	    #ForcedCounter.ForcedAct := #ForcedCounter.ForcedAct + 1;
	    
	    #EPICS.FB_Step := #STAT.P_Step;
	    IF (#EPICS.FB_Step < 0.1) THEN
	        #EPICS.FB_Step := 0.1;
	    END_IF;
	    
	    //SET the setpoint
	    IF (#STAT.Ramping) THEN
	        #STAT.RampSetPoint := #STAT.P_Setpoint;
	    ELSE
	        #STAT.PumpSP := #STAT.P_Setpoint;
	    END_IF;
	    
	    #STAT.FB_Setpoint := #STAT.P_Setpoint;
	    
	    //Force Analog Input
	    IF (#EPICS.Cmd_ForceValInp) THEN
	        #STAT.FB_ForceSpeed := #STAT.P_ForceSpeed;
	        #STAT.PumpSpd := #STAT.FB_ForceSpeed;
	    END_IF;
	    //Force analog Output
	    IF (#EPICS.Cmd_ForceValOut) THEN
	        #STAT.FB_Manipulated := #STAT.P_Manipulated;
	        #STAT.PumpMV := #STAT.FB_Manipulated;
	    END_IF;
	    
	    // Force start and stop
	    IF (#EPICS.Cmd_ManuStart) THEN
	        #STAT.Cmd_Run := TRUE;
	    ELSIF (#EPICS.Cmd_ManuStop) THEN
	        #STAT.Cmd_Run := FALSE;
	        #STAT.PumpMV := 0;
	    END_IF;
	    
	END_IF;
	
	//Send pump speed to EPICS
	#STAT.PumpSpeed := #STAT.PumpSpd;
	
	
	//Calculate Ramping spped
	IF (#STAT.Ramping) THEN
	    IF (#STAT.Accelerating) THEN // Ramping UP speed
	        #STAT.ActRampSpeed := #STAT.FB_RampUPRANGE / #STAT.FB_RampUPTIME;
	        IF (#STAT.ActRampSpeed > #EPICS.MaxRampUPSpd) THEN //Maximum Up limit
	            #STAT.ActRampSpeed := #EPICS.MaxRampUPSpd;
	        END_IF;
	    END_IF;
	    IF (#STAT.Decelerating) THEN // Ramping UP speed
	        #STAT.ActRampSpeed := #STAT.FB_RampDNRANGE / #STAT.FB_RampDNTIME;
	        IF (#STAT.ActRampSpeed > #EPICS.MaxRampDNSpd) THEN //Maximum Dn limit
	            #STAT.ActRampSpeed := #EPICS.MaxRampDNSpd;
	        END_IF;
	    END_IF;
	    IF (NOT #STAT.Accelerating) AND (NOT #STAT.Decelerating) THEN
	        #STAT.ActRampSpeed := 0.0;
	    END_IF;
	ELSE
	    #STAT.Accelerating := FALSE;
	    #STAT.Decelerating := FALSE;
	    #STAT.ActRampSpeed := 0.0;
	END_IF;
	
	
	//Ramping:
	//Input: RampSetPoint
	//Reference: PumpSP
	//Output: PumpSP
	//
	IF (#STAT.Ramping) AND (#STAT.GroupInterlock = FALSE) THEN
	    IF (ABS(#STAT.PumpSP - #STAT.RampSetPoint) >= (#STAT.ActRampSpeed / 10.0)) THEN // Devided by 10 because of 100ms pulse
	        IF ("Utilities".Pulse_100ms) THEN
	            IF (#STAT.PumpSP < #STAT.RampSetPoint) THEN
	                #STAT.Accelerating := TRUE;
	                #STAT.Decelerating := FALSE;
	                #STAT.PumpSP := #STAT.PumpSP + (#STAT.ActRampSpeed / 10.0);
	                IF (#STAT.PumpSP >= #STAT.RampSetPoint) THEN
	                    #STAT.PumpSP := #STAT.RampSetPoint;
	                    #STAT.Accelerating := FALSE;
	                    #STAT.Decelerating := FALSE;
	                END_IF;
	            ELSE
	                
	                IF (#STAT.PumpSP > #STAT.RampSetPoint) THEN
	                    #STAT.Accelerating := FALSE;
	                    #STAT.Decelerating := TRUE;
	                    #STAT.PumpSP := #STAT.PumpSP - (#STAT.ActRampSpeed / 10.0);
	                    IF (#STAT.PumpSP <= #STAT.RampSetPoint) THEN
	                        #STAT.PumpSP := #STAT.RampSetPoint;
	                        #STAT.Accelerating := FALSE;
	                        #STAT.Decelerating := FALSE;
	                    END_IF;
	                END_IF;
	            END_IF;
	        END_IF;
	    ELSE
	        #STAT.PumpSP := #STAT.RampSetPoint;
	        #STAT.Accelerating := FALSE;
	        #STAT.Decelerating := FALSE;
	    END_IF;
	END_IF;
	
	
	//Apply output in Auto and Manula modes
	IF (#STAT.OpMode_Auto OR #STAT.OpMode_Manual) THEN
	    #STAT.PumpMV := #STAT.PumpSP;
	END_IF;
	
	//OutPut of the block
	#UnScaleRetVal := UNSCALE(IN := #STAT.PumpMV, HI_LIM := 100, LO_LIM := 0, BIPOLAR := 0, OUT => #HWO_PumpMV);
	
	IF ((#STAT.OpMode_Auto = TRUE) OR (#STAT.OpMode_Manual = TRUE)) THEN
	    #STAT.FB_ForceSpeed := #STAT.PumpSpeed;
	    #STAT.FB_Manipulated := #STAT.PumpMV;
	END_IF;
	
	
	//Interlock handling
	#STAT.GroupInterlock := (#StartInterlock);
	#STAT.StartInterlock := #StartInterlock;
	
	//Pump reached the SetPoint
	#STAT.SpReachedTimer(IN := (((#STAT.PumpSpd <= #STAT.PumpSP + (#EPICS.DiscrPerc)) AND
	                     (#STAT.PumpSpd >= #STAT.PumpSP - (#EPICS.DiscrPerc))) AND NOT #STAT.Accelerating AND NOT #STAT.Decelerating),
	                     Q => #STAT.SPReached,
	                     PT := t#1s);
	
	//===========================================================================================================
	//Alarm handling
	//===========================================================================================================
	//Check discrepancy
	#IsDiscrepancy := FALSE;
	IF (#STAT.PumpSpd > #STAT.PumpMV + (#EPICS.DiscrPerc)) OR
	    (#STAT.PumpSpd < #STAT.PumpMV - (#EPICS.DiscrPerc)) THEN
	    #IsDiscrepancy := TRUE;
	END_IF;
	
	#STAT.DiscTimer(IN := #IsDiscrepancy,
	                PT := #EPICS.DiscrTime,
	                Q => #STAT.DiscrepancyAlarm);
	
	//Generate SS alarm
	IF (#SS_ON) THEN
	    #STAT.SSTriggered := TRUE;
	    #STAT.FB_Setpoint := #SS_Value;
	    #STAT.Ramping := FALSE;
	    #STAT.PumpSP := #SS_Value;
	    //Overwrite the last AutoSP so after the Failure Action when the device get back to Auto it stays in the SS speed.
	    #Auto_SetPoint := #SS_Value;
	    #STAT.RampSetPoint := #SS_Value;
	END_IF;
	
	//Force Latching if SS was triggered
	IF (#STAT.SSTriggered) THEN
	    IF (NOT #EPICS.LatchAlarm) THEN
	        #EPICS.LatchAlarm := TRUE;
	    END_IF;
	END_IF;
	
	//Reset Alarms
	IF (#EPICS.Cmd_AckAlarm OR #GlobalAlarmAck) THEN
	    #EPICS.GroupAlarm := FALSE;
	    #EPICS.IO_Error := FALSE;
	    #EPICS.SPDiscrepancy := FALSE;
	    #EPICS.ModuleDiagError := FALSE;
	    #STAT.SSTriggered := FALSE;
	END_IF;
	
	//Latched Alarm
	IF (#EPICS.LatchAlarm) THEN
	    //Discrepancy
	    IF (#STAT.DiscrepancyAlarm) THEN
	        #EPICS.SPDiscrepancy := TRUE;
	    END_IF;
	    //Input Module ERROR
	    IF (#DRIVE_HW_Diag) THEN
	        #EPICS.ModuleDiagError := TRUE;
	    END_IF;
	    //IO Error
	    IF (#HWI_Speed < -500) OR (#HWI_Speed > 28000) THEN
	        #EPICS.IO_Error := TRUE;
	    END_IF;
	    
	    IF (#EPICS.IO_Error OR
	        #EPICS.SPDiscrepancy OR
	        #EPICS.ModuleDiagError OR
	        #STAT.SSTriggered) THEN
	        #EPICS.GroupAlarm := TRUE;
	    END_IF;
	    
	ELSE //Not Latched Alarm
	    
	    //Discrepancy
	    #EPICS.SPDiscrepancy := #STAT.DiscrepancyAlarm;
	    //Input Module ERROR
	    #EPICS.ModuleDiagError := #DRIVE_HW_Diag;
	    //IO ERROR
	    #EPICS.IO_Error := (#HWI_Speed < -500) OR (#HWI_Speed > 28000);
	    
	    #EPICS.GroupAlarm := (#EPICS.IO_Error OR
	    #EPICS.SPDiscrepancy OR
	    #EPICS.ModuleDiagError OR
	    #STAT.SSTriggered);
	END_IF;
	
	//When is the analog Pump Start/Stop
	#PumpRunning := (#STAT.PumpSpd >= #STAT.PumpRunningPercentage) AND #STAT.SPReached;
	#PumpStopped := (#STAT.PumpSpd <= #STAT.PumpStoppedPercentage) AND #STAT.SPReached;
	
	//===========================================================================================================
	// Map KSB Outputs
	#KSB.udtKSB_RDP_WR.bOutRun := #STAT.Cmd_Run;
	#KSB.udtKSB_RDP_WR.bOutPump_Check := FALSE;
	#KSB.udtKSB_RDP_WR.bOutReset_Fault := #EPICS.Cmd_AckAlarm OR #GlobalAlarmAck;
	
	#rMemSetPoint := #STAT.PumpMV * 100;
	#iTmpSetpoint := REAL_TO_UINT(#rMemSetPoint);
	
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X0 := #iTmpSetpoint.%X8;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X1 := #iTmpSetpoint.%X9;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X2 := #iTmpSetpoint.%X10;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X3 := #iTmpSetpoint.%X11;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X4 := #iTmpSetpoint.%X12;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X5 := #iTmpSetpoint.%X13;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X6 := #iTmpSetpoint.%X14;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_1.%X7 := #iTmpSetpoint.%X15;
	
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X0 := #iTmpSetpoint.%X0;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X1 := #iTmpSetpoint.%X1;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X2 := #iTmpSetpoint.%X2;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X3 := #iTmpSetpoint.%X3;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X4 := #iTmpSetpoint.%X4;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X5 := #iTmpSetpoint.%X5;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X6 := #iTmpSetpoint.%X6;
	#KSB.udtKSB_RDP_WR.iOutSetpoint_2.%X7 := #iTmpSetpoint.%X7;
	
	IF (#EPICS.Cmd_BusControl AND #STAT.OpMode_Forced) THEN  // Set Drive AUTO Mode in PLC Forced Mode only
	    #KSB.udtKSB_RDP_WR.bOutBus_Control_Mode := TRUE;
	    #KSB.udtKSB_RDP_WR.byOutFeedback_Control_Mode := 128;
	    #KSB.udtKSB_RDP_WR.byOutControl_Mode := 130;
	END_IF;
	
	IF (#FallingEdge_DriveAuto.Q) THEN
	    #KSB.udtKSB_RDP_WR.bOutRun := FALSE;
	    #KSB.udtKSB_RDP_WR.bOutBus_Control_Mode := FALSE;
	    #KSB.udtKSB_RDP_WR.byOutControl_Mode := #KSB.udtKSB_RDP_RD.byInControl_Mode;
	END_IF;
	
	//=============================================================================
	//  WRITE Drive Parameters
	#iStKSB_RDPWRRetVal := DPWR_DAT(LADDR := #hwKSBRotoDynamicWR, RECORD := #KSB.udtKSB_RDP_WR);
	
	//Take back INPUTS so they never got stuck
	#AutoStart := FALSE;
	#AutoStop := FALSE;
	#ForceAutoMode := FALSE;
	#ForceManualMode := FALSE;
	#StartInterlock := FALSE;
	#SS_ON := FALSE;
	
	#STAT.AutoSetpointInternal := #Auto_SetPoint;
	
	
	//Enable/Disable Block Icon Control
	#EPICS.EnableBlkCtrl := #EnableBlkIconControl AND #STAT.OpMode_Manual;
	
	
	//Pump color (0=BLUE; 1=WHITE; 2=RED; 3=GRAY; 4=GREEN)
	IF (#EPICS.GroupAlarm) THEN
	    #EPICS.PumpColor := 2;
	ELSE
	    IF (#STAT.PumpSpeed > 5.0) THEN
	        #EPICS.PumpColor := 1;
	    ELSE
	        IF (#STAT.PumpSpeed < 5.0) THEN
	            #EPICS.PumpColor := 0;
	        ELSE
	            #EPICS.PumpColor := 0;
	        END_IF;
	    END_IF;
	END_IF;
	
	//Copy variables to EPICS
	#EPICS.OpMode_Auto := #STAT.OpMode_Auto;
	#EPICS.OpMode_Manual := #STAT.OpMode_Manual;
	#EPICS.OpMode_Forced := #STAT.OpMode_Forced;
	#EPICS.InterlockMsg := #InterlockMsg;
	#EPICS.PumpSpeed := #STAT.PumpSpeed;
	#EPICS.PumpMV := #STAT.PumpMV;
	#EPICS.PumpSP := #STAT.PumpSP;
	#PumpActSpd := #STAT.PumpSpeed;
	#PumpSetpoint := #STAT.PumpSP;
	#EPICS.RampSettingOK := #STAT.RampSettingOK;
	#EPICS.Ramping := #STAT.Ramping;
	#EPICS.Accelerating := #STAT.Accelerating;
	#EPICS.Decelerating := #STAT.Decelerating;
	#EPICS.ActRampSpeed := #STAT.ActRampSpeed;
	#EPICS.StartInterlock := #STAT.StartInterlock;
	#EPICS.FB_Setpoint := #STAT.FB_Setpoint;
	#EPICS.FB_RampUPRANGE := #STAT.FB_RampUPRANGE;
	#EPICS.FB_RampUPTIME := #STAT.FB_RampUPTIME;
	#EPICS.FB_RampDNRANGE := #STAT.FB_RampDNRANGE;
	#EPICS.FB_RampDNTIME := #STAT.FB_RampDNTIME;
	#EPICS.FB_ForceSpeed := #STAT.FB_ForceSpeed;
	#EPICS.FB_Manipulated := #STAT.FB_Manipulated;
	#EPICS.GroupInterlock := #STAT.GroupInterlock;
	#EPICS.SSTriggered := #STAT.SSTriggered;
	
END_FUNCTION_BLOCK

