﻿FUNCTION_BLOCK "hndWCK12_TT"
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT 
      HWI_RAWTemperature { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Int;
      IO_Module_Diag { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;
      GlobalAlarmAck { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Bool;
   END_VAR

   VAR_OUTPUT 
      CurrentENG { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;
      TemperatureENG { ExternalAccessible := 'False'; ExternalVisible := 'False'; ExternalWritable := 'False'} : Real;
   END_VAR

   VAR_IN_OUT 
      EPICS : "DEVTYPE_ICS_WCK12_TT";
      ForcedCounter : "udtForcedCounter";
      Alarms : "udtAlarms";
   END_VAR

   VAR 
      TIM_LOLO {InstructionName := 'TON_TIME'; LibVersion := '1.0'} : TON_TIME;
      TIM_LO {InstructionName := 'TON_TIME'; LibVersion := '1.0'} : TON_TIME;
      TIM_HI {InstructionName := 'TON_TIME'; LibVersion := '1.0'} : TON_TIME;
      TIM_HIHI {InstructionName := 'TON_TIME'; LibVersion := '1.0'} : TON_TIME;
      TON_LOLO_Condition { S7_SetPoint := 'True'} : Bool;
      TON_LO_Condition : Bool;
      TON_HI_Condition : Bool;
      TON_HIHI_Condition : Bool;
   END_VAR

   VAR_TEMP 
      RET_VAL : Word;
      RET_VAL2 : Word;
   END_VAR


BEGIN
	// ###################################### ICS HWI ###############################################
	// ######################  ICS Water Cooling Instrument Library     #############################
	// ######## Moderator-Reflector and Shielding and Plugs Water Cooling Control System ############
	// ##  PLC Sample Code in VersionDog: ICS_LIBRARY_TARGET_MR_SPCS                               ## 
	// ##  CCDB device types: ICS_xxxxx                                                            ##  
	// ##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Project: Target - =ESS.TS.CUP.K01/2         ##
	// ##                                                                                          ##  
	// ##                                TT - Temperature Transmitter                              ##
	// ##                                                                                          ##  
	// ##                                                                                          ##  
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se; emilio.asensi@esss.se
	//
	//Functionality: TT Transmitter handler function block
	// 
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//01.00.00 2019-06-24   ESS/ICS               First version 
	//02.00.00 2020-02-24   Miklos Boros          Tested with real HW
	//03.00.00 2020-05-21   Marino Vojneski       Removed locking section, code compatible with ICS_WCK12_TT.def
	//03.01.00 2020-07-29   Marino Vojneski       Added logic for alarm delay and hysteresis
	//=============================================================================
	//
	//
	
	//HMI Faceplate - LIMITS to EPICS
	//#EPICS.FB_Absolute := #Alarms.Absolute;    //  In future: Needs SNL to make this working properly from the user interface
	#EPICS.FB_Absolute := #EPICS.P_Absolute;
	#EPICS.FB_AlarmSetpoint := #EPICS.P_AlarmSetpoint;
	#EPICS.FB_Delay := #EPICS.P_Delay;
	
	IF (#EPICS.FB_Absolute) THEN    //  If alarm thresholds are absolute
	    #EPICS.FB_Hysteresis := (#EPICS.P_Hysteresis / 100) * ABS(#Alarms.ScaleHIGH - #Alarms.ScaleLOW);
	    #EPICS.FB_Limit_HIHI := #EPICS.P_Limit_HIHI;
	    #EPICS.FB_Limit_HI := #EPICS.P_Limit_HI;
	    #EPICS.FB_Limit_LO := #EPICS.P_Limit_LO;
	    #EPICS.FB_Limit_LOLO := #EPICS.P_Limit_LOLO;
	ELSE
	    #EPICS.FB_Hysteresis := (#EPICS.P_Hysteresis / 100) * ABS(#EPICS.FB_AlarmSetpoint - #Alarms.ScaleLOW);
	    
	    IF (#EPICS.P_Limit_HIHI > 0) THEN
	        #EPICS.FB_Limit_HIHI := #EPICS.FB_AlarmSetpoint + ((#EPICS.P_Limit_HIHI / 100) * #EPICS.FB_AlarmSetpoint);
	    ELSE
	        #EPICS.FB_Limit_HIHI := #Alarms.ScaleHIGH;
	    END_IF;
	    
	    IF (#EPICS.P_Limit_HI > 0) THEN
	        #EPICS.FB_Limit_HI := #EPICS.FB_AlarmSetpoint + ((#EPICS.P_Limit_HI / 100) * #EPICS.FB_AlarmSetpoint);
	    ELSE
	        #EPICS.FB_Limit_HI := #Alarms.ScaleHIGH;
	    END_IF;
	    
	    IF (#EPICS.P_Limit_LO > 0) THEN
	        #EPICS.FB_Limit_LO := #EPICS.FB_AlarmSetpoint - ((#EPICS.P_Limit_LO / 100) * #EPICS.FB_AlarmSetpoint);
	    ELSE
	        #EPICS.FB_Limit_LO := #Alarms.ScaleLOW;
	    END_IF;
	    
	    IF (#EPICS.P_Limit_LOLO > 0) THEN
	        #EPICS.FB_Limit_LOLO := #EPICS.FB_AlarmSetpoint - ((#EPICS.P_Limit_LOLO / 100) * #EPICS.FB_AlarmSetpoint);
	    ELSE
	        #EPICS.FB_Limit_LOLO := #Alarms.ScaleLOW;
	    END_IF;
	END_IF;
	
	//Overwrite alarms according to SCALE limits¨
	#EPICS.ScaleHIGH := #Alarms.ScaleHIGH;
	#EPICS.ScaleLOW := #Alarms.ScaleLOW;
	
	IF (#EPICS.FB_Limit_HIHI > #Alarms.ScaleHIGH) THEN
	    #EPICS.FB_Limit_HIHI := #Alarms.ScaleHIGH;
	END_IF;
	
	IF (#EPICS.FB_Limit_HI > #EPICS.FB_Limit_HIHI) THEN
	    #EPICS.FB_Limit_HI := #EPICS.FB_Limit_HIHI;
	END_IF;
	
	IF (#EPICS.FB_Limit_LOLO < #Alarms.ScaleLOW) THEN
	    #EPICS.FB_Limit_LOLO := #Alarms.ScaleLOW;
	END_IF;
	
	IF (#EPICS.FB_Limit_LO < #EPICS.FB_Limit_LOLO) THEN
	    #EPICS.FB_Limit_LO := #EPICS.FB_Limit_LOLO;
	END_IF;
	
	IF (#EPICS.FB_Limit_LO > #EPICS.FB_Limit_HI) THEN
	    #EPICS.FB_Limit_LO := #EPICS.FB_Limit_HI - #EPICS.FB_Hysteresis;
	END_IF;
	
	//  Copy EPICS Setup to DB
	#Alarms.Absolute := #EPICS.FB_Absolute;
	#Alarms.Hysteresis := #EPICS.FB_Hysteresis;
	#Alarms.LOLO.Delay := #EPICS.FB_Delay;
	#Alarms.LOLO.Value := #EPICS.FB_Limit_LOLO;
	#Alarms.LO.Delay := #EPICS.FB_Delay;
	#Alarms.LO.Value := #EPICS.FB_Limit_LO;
	#Alarms.HI.Delay := #EPICS.FB_Delay;
	#Alarms.HI.Value := #EPICS.FB_Limit_HI;
	#Alarms.HIHI.Delay := #EPICS.FB_Delay;
	#Alarms.HIHI.Value := #EPICS.FB_Limit_HIHI;
	#Alarms.Setpoint := #EPICS.FB_AlarmSetpoint;
	
	//HMI Faceplate - OP MODES - AUTO
	IF (#EPICS.Cmd_Auto) THEN
	    #EPICS.OpMode_Auto := TRUE;
	    #EPICS.OpMode_Forced := FALSE;
	END_IF;
	
	//HMI Faceplate - OP MODES - FORCED
	IF (#EPICS.Cmd_Force) AND (#EPICS.Inhibit_Force = FALSE) THEN
	    #EPICS.OpMode_Auto := FALSE;
	    #EPICS.OpMode_Forced := TRUE;
	END_IF;
	
	//If there is no Operation Mode, set the mode to Manual
	IF (#EPICS.OpMode_Auto = FALSE) AND (#EPICS.OpMode_Forced = FALSE) THEN
	    #EPICS.OpMode_Auto := TRUE;
	    #EPICS.OpMode_Forced := FALSE;
	END_IF;
	
	
	//AUTO MODE
	IF (#EPICS.OpMode_Auto = TRUE) THEN
	    
	    //Current Scaling
	    
	    #RET_VAL := SCALE(IN := #HWI_RAWTemperature, HI_LIM := 20, LO_LIM := 4, BIPOLAR := 0, OUT => #CurrentENG);
	    #RET_VAL2 := SCALE(IN := #HWI_RAWTemperature, HI_LIM := #Alarms.ScaleHIGH, LO_LIM := #Alarms.ScaleLOW, BIPOLAR := 0, OUT => #TemperatureENG); //Kelvin
	    
	    
	    IF (#HWI_RAWTemperature < 0) OR (#HWI_RAWTemperature > 27648) THEN
	        #EPICS.Module_Error := TRUE;
	    ELSE
	        #EPICS.Module_Error := FALSE;
	    END_IF;
	    
	    #EPICS.EnableAutoBtn := FALSE;
	    #EPICS.EnableForcedBtn := TRUE;
	    #EPICS.FB_ForceValue := #TemperatureENG;
	    #EPICS.Temperature := #TemperatureENG;
	    
	    #EPICS.Current := #CurrentENG;
	    
	    
	END_IF;
	
	
	//Overrange / Underrange check
	IF (#HWI_RAWTemperature < 0) THEN
	    #EPICS.Underrange := TRUE;
	ELSE
	    #EPICS.Underrange := FALSE;
	END_IF;
	
	IF (#HWI_RAWTemperature > 27648) THEN
	    #EPICS.Overrange := TRUE;
	ELSE
	    #EPICS.Overrange := FALSE;
	END_IF;
	
	#ForcedCounter.ForcedMax := #ForcedCounter.ForcedMax + 1;
	
	
	//FORCE MODE
	IF (#EPICS.OpMode_Forced = TRUE) THEN
	    #EPICS.EnableAutoBtn := TRUE;
	    #EPICS.EnableForcedBtn := FALSE;
	    
	    #ForcedCounter.ForcedAct := #ForcedCounter.ForcedAct + 1;
	    //#EPICS.Range := 0;
	    
	    
	    IF (#EPICS.Cmd_ForceVal) THEN
	        #TemperatureENG := #EPICS.P_ForceValue;
	        #EPICS.FB_ForceValue := LREAL_TO_REAL(IN := #TemperatureENG);
	        #EPICS.Temperature := LREAL_TO_REAL(IN := #TemperatureENG);
	    END_IF;
	END_IF;
	
	
	//Alarm handling
	
	IF (#EPICS.Temperature > #EPICS.FB_Limit_HIHI) THEN
	    #TON_HIHI_Condition := TRUE;
	ELSE
	    #TON_HIHI_Condition := FALSE;
	END_IF;
	IF (#EPICS.Temperature > #EPICS.FB_Limit_HI) THEN
	    #TON_HI_Condition := TRUE;
	ELSE
	    #TON_HI_Condition := FALSE;
	END_IF;
	IF (#EPICS.Temperature < #EPICS.FB_Limit_LO) THEN
	    #TON_LO_Condition := TRUE;
	ELSE
	    #TON_LO_Condition := FALSE;
	END_IF;
	IF (#EPICS.Temperature < #EPICS.FB_Limit_LOLO) THEN
	    #TON_LOLO_Condition := TRUE;
	ELSE
	    #TON_LOLO_Condition := FALSE;
	END_IF;
	
	#TIM_HIHI(IN := #TON_HIHI_Condition,
	          PT := INT_TO_TIME(#EPICS.FB_Delay * 1000));
	#TIM_HI(IN := #TON_HI_Condition,
	        PT := INT_TO_TIME(#EPICS.FB_Delay * 1000));
	#TIM_LO(IN := #TON_LO_Condition,
	        PT := INT_TO_TIME(#EPICS.FB_Delay * 1000));
	#TIM_LOLO(IN := #TON_LOLO_Condition,
	          PT := INT_TO_TIME(#EPICS.FB_Delay * 1000));
	
	IF (#EPICS.Cmd_AckAlarm) OR #GlobalAlarmAck THEN
	    #EPICS.GroupAlarm := FALSE;
	    #EPICS.Underrange := FALSE;
	    #EPICS.Overrange := FALSE;
	    IF (#EPICS.Temperature < #EPICS.FB_Limit_HIHI - #EPICS.FB_Hysteresis) THEN
	        #EPICS.HIHI := FALSE;
	    END_IF;
	    // IF (#EPICS.Temperature < #EPICS.FB_Limit_HI - #EPICS.FB_Hysteresis)
	    //  #EPICS.HI := FALSE;
	    //  END_IF;
	    IF (#EPICS.Temperature > #EPICS.FB_Limit_LOLO + #EPICS.FB_Hysteresis) THEN
	        #EPICS.LOLO := FALSE;
	    END_IF;
	    // IF (#EPICS.Temperature > #EPICS.FB_Limit_LO + #EPICS.FB_Hysteresis)
	    //  #EPICS.LO := FALSE;
	    //  END_IF;
	    #EPICS.IO_Error := FALSE;
	    #EPICS.Module_Error := FALSE;
	END_IF;
	
	IF (#EPICS.LatchAlarm) THEN
	    //IO_Module_ERROR
	    IF (#IO_Module_Diag) THEN
	        #EPICS.Module_Error := TRUE;
	    END_IF;
	    
	    #EPICS.HI := FALSE;
	    #EPICS.LO := FALSE;
	    IF (#TIM_LOLO.Q) THEN
	        #EPICS.LOLO := TRUE;
	    ELSE
	        IF (#TIM_LO.Q) THEN
	            #EPICS.LO := TRUE;
	        ELSE
	            IF (#TIM_HIHI.Q) THEN
	                #EPICS.HIHI := TRUE;
	            ELSE
	                IF (#TIM_HI.Q) THEN
	                    #EPICS.HI := TRUE;
	                END_IF;
	            END_IF;
	        END_IF;
	    END_IF;
	    //Group ERROR
	    IF (#EPICS.IO_Error OR
	        #EPICS.Module_Error OR
	        #EPICS.Overrange OR
	        #EPICS.Underrange OR
	        #EPICS.HIHI OR
	        #EPICS.LOLO) THEN
	        #EPICS.GroupAlarm := TRUE;
	    END_IF;
	    
	ELSE // NOT Latched
	    IF (#TIM_LOLO.Q) THEN
	        #EPICS.HIHI := FALSE;
	        #EPICS.HI := FALSE;
	        #EPICS.LO := FALSE;
	        #EPICS.LOLO := TRUE;
	    ELSE
	        IF (#TIM_LO.Q) AND (#EPICS.Temperature > #EPICS.FB_Limit_LOLO + #EPICS.FB_Hysteresis) THEN
	            #EPICS.HIHI := FALSE;
	            #EPICS.HI := FALSE;
	            #EPICS.LO := TRUE;
	            #EPICS.LOLO := FALSE;
	        ELSE
	            IF (#TIM_HIHI.Q) THEN
	                #EPICS.HIHI := TRUE;
	                #EPICS.HI := FALSE;
	                #EPICS.LO := FALSE;
	                #EPICS.LOLO := FALSE;
	            ELSE
	                IF (#TIM_HI.Q) AND (#EPICS.Temperature < #EPICS.FB_Limit_HIHI - #EPICS.FB_Hysteresis) THEN
	                    #EPICS.HIHI := FALSE;
	                    #EPICS.HI := TRUE;
	                    #EPICS.LO := FALSE;
	                    #EPICS.LOLO := FALSE;
	                ELSE
	                    #EPICS.HIHI := FALSE;
	                    #EPICS.HI := FALSE;
	                    #EPICS.LO := FALSE;
	                    #EPICS.LOLO := FALSE;
	                END_IF;
	            END_IF;
	        END_IF;
	    END_IF;
	    //IO_Module_ERROR
	    #EPICS.Module_Error := #IO_Module_Diag;
	    
	    //Group ERROR
	    #EPICS.GroupAlarm := (#EPICS.IO_Error OR
	    #EPICS.Module_Error OR
	    #EPICS.Overrange OR
	    #EPICS.Underrange OR
	    #EPICS.HIHI OR
	    #EPICS.LOLO);
	    
	END_IF;
END_FUNCTION_BLOCK

