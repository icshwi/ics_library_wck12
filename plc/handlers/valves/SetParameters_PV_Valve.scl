﻿FUNCTION "SetParameters_PV_Valve" : Void
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT 
      AlarmLatching : Bool;   // Handler Alarm latching ON/OFF (ack needed or not)
      Inhibit_Manual : Bool;   // Inhibit Manual mode (only auto)
      Inhibit_Force : Bool;   // Inhibit Force mode
      Discrepancy_OpeningTime : Time;   // Discrepancy allowed time to Open
      Discrepancy_ClosingTime : Time;   // Discrepancy allowed time to Close
      NormallyOpenValve : Bool;   // 0=NormallyClosed 1=NormallyOpened
   END_VAR

   VAR_IN_OUT 
      PV_Valve_Handler_STAT : "Handle_Valve_PV_STAT";
      PV_Valve_EPICS_Instance : "DEVTYPE_ICS_WCK12_PV";
   END_VAR


BEGIN
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se
	//
	//Functionality: Apply the parameters on the PV Valve
	//               This function is needed to make sure all parameters are initialized
	// 
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//01.00.00 2020-05-18   Miklos Boros          First version 
	//=============================================================================
	//
	//
	//
	
	#PV_Valve_EPICS_Instance.LatchAlarm := #AlarmLatching;   
	#PV_Valve_EPICS_Instance.Inhibit_Manual := #Inhibit_Manual;
	#PV_Valve_EPICS_Instance.Inhibit_Force := #Inhibit_Force;
	#PV_Valve_EPICS_Instance.OpeningTime := #Discrepancy_OpeningTime;
	#PV_Valve_EPICS_Instance.ClosingTime := #Discrepancy_ClosingTime;
	#PV_Valve_Handler_STAT.NormallyOpenValve := #NormallyOpenValve;
	
END_FUNCTION

