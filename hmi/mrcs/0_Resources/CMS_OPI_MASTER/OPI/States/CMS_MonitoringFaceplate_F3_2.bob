<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>CMS F3_2 ColdBox High Pressure Failure Action</name>
  <macros>
    <PLCName>CrS-CMS:Cryo-PLC-01</PLCName>
  </macros>
  <width>1780</width>
  <height>1100</height>
  <background_color>
    <color name="WHITE" red="255" green="255" blue="255">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>F3_2 - ColdBox High Pressure Failure Action</text>
    <y>90</y>
    <width>1780</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>Block Icons</text>
    <x>1410</x>
    <y>136</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>Expected Failure Action Events after F3_2 is executed</text>
    <x>30</x>
    <y>136</y>
    <width>1350</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>WDF2</name>
    <macros>
      <Faceplate>../../../CMS_OPI_MASTER/OPI/States/CMS_MonitoringFaceplate_M3_2.bob</Faceplate>
      <StepName>ColdBox Vacuum Monitoring</StepName>
      <WIDDev>Virt</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>M32</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_BlockIcon_MonitoringFunction_Compact.bob</file>
    <x>1410</x>
    <y>190</y>
    <width>330</width>
    <height>160</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>WDF5_1</name>
    <macros>
      <StepName>ColdBox Vacuum Failure Action</StepName>
      <WIDDev>Virt</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>F32</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_BlockIcon_FailureAction_Compact.bob</file>
    <x>1410</x>
    <y>360</y>
    <width>330</width>
    <height>160</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../CMS_Header.bob</file>
    <width>2080</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>FailureAction1</name>
    <pv_name>${StepDeviceName}:Actions1</pv_name>
    <x>30</x>
    <y>190</y>
    <width>660</width>
    <height>630</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="18.0">
      </font>
    </font>
    <labels>
      <text>Y1 - CV-42310 CLOSED</text>
      <text>Y10 - CV-42320 CLOSED</text>
      <text>Y7 - CV-42330 CLOSED</text>
      <text>Y6 - PID-62001 PID Overpressure Control Active</text>
      <text>Y8 - CV-62005 CLOSED</text>
      <text>PV-42820 CLOSED</text>
      <text>PV-42860 CLOSED</text>
      <text>DP-42150 STOPPED</text>
      <text>PV-42540 CLOSED</text>
      <text>PV-42900 CLOSED</text>
      <text>PV-42580 CLOSED</text>
      <text>PV-42620 CLOSED</text>
      <text>PV-42660 CLOSED</text>
      <text>PV-42700 CLOSED</text>
      <text>PV-42740 CLOSED</text>
      <text>PV-42780 CLOSED</text>
    </labels>
    <tooltip>RED = still waiting for feedback. GREEN = Failure Action executed</tooltip>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>FailureAction2</name>
    <pv_name>${StepDeviceName}:Actions2</pv_name>
    <x>720</x>
    <y>190</y>
    <width>660</width>
    <height>630</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="18.0">
      </font>
    </font>
    <labels>
      <text>Y2 - CV-62029 CLOSED and ColdBox Heaters Stopped</text>
      <text></text>
      <text>Y4 - CV-62037 CLOSED</text>
      <text>PV-69030 CLOSED</text>
      <text>TP-69100 STOPPED</text>
      <text>DP-69120 STOPPED</text>
      <text>P-62010 STOPPED</text>
      <text>P-62011 STOPPED</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
    <tooltip>RED = still waiting for feedback. GREEN = Failure Action executed</tooltip>
  </widget>
</display>
